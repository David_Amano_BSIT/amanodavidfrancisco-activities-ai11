package com.example.color;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    View screenView;
    Button clickMe;
    int[] color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        color = new int[] {Color.RED, Color. GREEN, Color. BLUE, Color. BLACK, Color. YELLOW, Color. GRAY};

        screenView = findViewById(R.id.rView);
        clickMe = (Button) findViewById(R.id.button);

        clickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int arylength = color.length;

                Random random = new Random();
                int rNum = random.nextInt(arylength);

                screenView.setBackgroundColor(color[rNum]);
            }
        });


    }
}